package net.mytabletop.avaloqa.timezone.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneData {
	Date tzDate;
	String tzTimeZone;
	
	public TimeZoneData() {
		tzDate = java.util.Calendar.getInstance().getTime();
		tzTimeZone = TimeZone.getDefault().getID();
	}
	
	public String getNowJsonString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat tf = new SimpleDateFormat("HH:mm:ss");
		
		String date = df.format(tzDate);
		String time = tf.format(tzDate);
		
		String stringData = 
				"{" 
				+ "\"SysDate\":\"" + date + "\"," 
				+ "\"SysTime\":\"" + time + "\"," 
				+ "\"UnixTime\":\"" + System.currentTimeMillis() / 1000L  + "\"," 
				+ "\"SysTZ\":\"" + tzTimeZone + "\"" 
				+"}"; 
		return stringData;
	}
}
