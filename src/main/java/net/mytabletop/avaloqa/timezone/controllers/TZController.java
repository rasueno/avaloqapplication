package net.mytabletop.avaloqa.timezone.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.mytabletop.avaloqa.timezone.models.TimeZoneData;

@RestController
public class TZController {
	
	//@GetMapping("/getTZDateTime", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/getTZDateTime", method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
	public String getTZDateTimeZone() {
		TimeZoneData tzData = new TimeZoneData();
		
		return tzData.getNowJsonString();
	}
}
