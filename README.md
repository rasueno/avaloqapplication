# Raffy sends the timezone
A simple spring boot app that exposes an endpoint to get the current server's date, time, and timezone.

## Available Endpoints
| Endpoint | DataType |Description |
|:-----------|:------------|:---------|
|/getTZDateTime|JSON (String)|Returns the current system date, time, and Timezone as a JSON formatted string. |

## How it works
The application is a simple springboot app that exposes the /getTZDateTime through the ***TZController*** class (***RestController***). The class has only one function as an endpoint ***getTZDateTimeZone*** which returns a JSON formatted string of the required data.

	@RestController
    public class TZController {
        
        @GetMapping("/getTZDateTime")
        public String getTZDateTimeZone() {
            TimeZoneData tzData = new TimeZoneData();
            
            return tzData.getNowJsonString();
        }
    }
All of the calculations and string manipulation is handled by the ***TimeZoneData*** object.
	
	public  class  TimeZoneData  {
		// ...
	}

## Running the application
This is a maven spring boot project, package via maven as any spring boot app and a runnable jar should be present on the target directory. Simply execute the complied output with java as any normal spring boot application.

	java -jar timezone-0.0.1-SNAPSHOT.jar &

Application would be listening at the default tomcat port of 8080.